package service;


import java.util.List;

public interface INoteBookService {

   List<Object> CalculateWordFrequency (String entry, String givenWord);

   int calculate(String x, String y);
}

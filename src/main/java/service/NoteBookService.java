package service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class NoteBookService implements INoteBookService{

    @Autowired
    private NoteBookService(){

    }

    @Override
    public List<Object> CalculateWordFrequency(String entry, String givenWord) {
        List<Object> similarWords = new ArrayList<>();
        AtomicInteger frequency = new AtomicInteger (0);
        Map<String, Integer> wordCount = Arrays.stream(entry.split("[\\s+]"))
                .collect(Collectors.toMap(k -> k, v -> 1, (v1, v2) -> v1 + v2));
        wordCount.forEach((k, v) -> {
            if(k.equalsIgnoreCase(givenWord)) {
                frequency.set(v);
                System.out.print("frequency" + frequency);
            }
            else {
                int d= calculate(k, givenWord);
                if (d<=1) similarWords.add(k);
            }
        });
        System.out.print("list of similar words" + similarWords.toString());
        similarWords.add(frequency.get());
        return similarWords;
    }

    public  int costOfSubstitution(char a, char b) {
        return a == b ? 0 : 1;
    }

    public  int min(int... numbers) {
        return Arrays.stream(numbers)
                .min().orElse(Integer.MAX_VALUE);
    }

    @Override
    public  int calculate(String x, String y) {
        int[][] dp = new int[x.length() + 1][y.length() + 1];

        for (int i = 0; i <= x.length(); i++) {
            for (int j = 0; j <= y.length(); j++) {
                if (i == 0) {
                    dp[i][j] = j;
                }
                else if (j == 0) {
                    dp[i][j] = i;
                }
                else {
                    dp[i][j] = min(dp[i - 1][j - 1]
                                    + costOfSubstitution(x.charAt(i - 1), y.charAt(j - 1)),
                            dp[i - 1][j] + 1,
                            dp[i][j - 1] + 1);
                }
            }
        }
        return dp[x.length()][y.length()];
    }
}

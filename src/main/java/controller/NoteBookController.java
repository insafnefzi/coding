package controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.INoteBookService;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/challenge/noteBook")
public class NoteBookController {


    private final INoteBookService iNoteBookService;

    @Autowired
    private NoteBookController(INoteBookService iNoteBookService){

        this.iNoteBookService = iNoteBookService;
    }


    @GetMapping(value = "/getFrequency")
    public List<Object>  getFrequency(@RequestParam String entry, @RequestParam String givenWord) {
        return iNoteBookService.CalculateWordFrequency(entry, givenWord);
    }
    @GetMapping(value = "/getString")
    public String  getString() {
        return "helloe";
    }





}

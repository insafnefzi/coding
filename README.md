# Introduction
this small application allows user to provide entry notebook and a given word,
and the application will calculate the frequency of the given word and provide
the list of similar words.

# Getting Started
run this application with java 17 and mvn 2.6.4.

# Scope of the solution

this solution provides a rest API that returns a list of object that contains 
the list of similar words and the number of frequency.

The amount of time spent on it: 4h.

this solution can be tested with postman with the url:
http://localhost:8080/api/challenge/noteBook/getFrequency?entry=Word Words Wor word&givenWord=Word

for an entry with the text  "Word Words Wor word"
and a given word "Word"

# Bring the solution further

Had I had more time, I would have created an angular application that 
consumes the rest API endpoint and would allow the user to enter the 
noteBook entry and the given word, after that he clicks on a button 
that would display the number of frequency and the list of similar words.


# Fullstack candidate: 
I have created an index.html that conatins 2 inputs where the user would 
put his data from notebook entry to given word and a button to give the 
information needed.

when running the application then on the navigator go to http://localhost:8080/
you should be able to see the index page.